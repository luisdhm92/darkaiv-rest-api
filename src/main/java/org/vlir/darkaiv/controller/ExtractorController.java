package org.vlir.darkaiv.controller;

import com.sun.jersey.multipart.FormDataParam;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.logging.Level;

import javax.ws.rs.Consumes;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.MediaType;
//import com.sun.jersey.multipart.FormDataParam;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.vlir.darkaiv.model.Document;
import org.vlir.darkaiv.rest.util.DarkaivRestUtils;

//import org.vlir.darkaiv.model.Employee;
import org.vlir.darkaiv.services.DataServices;
import org.vlir.darkaiv.services.ExtractionServicesImpl;

@RestController
@RequestMapping("/document")
public class ExtractorController {

    @Autowired
    DataServices dataServices;

    @Autowired
//    ExtractionServices extractionServices;
    ExtractionServicesImpl extractionServices;

    static final Logger logger = Logger.getLogger(ExtractorController.class);

    public ExtractorController() {
//        File file = new File(".");
//        System.out.println("Actual path " + file.getAbsolutePath());
//        AbstractApplicationContext context = new ClassPathXmlApplicationContext("webapp/WEB-INF/spring-config.xml");
//        extractionServices = (ExtractionServicesImpl) context.getBean("extractionServices");
    }

    @RequestMapping(value = "/workflow/test", method = RequestMethod.GET)
    public @ResponseBody
    String workflowTest() {
//        return "Hello world";

        File folder = new File("D:\\Daniel\\Belgium_Work\\Develop\\github\\DarkaivRestApi\\Internet and Higher Education");
        File[] listOfFiles = folder.listFiles();
        InputStream[] inStreams = new InputStream[listOfFiles.length];
        for (int i = 0; i < inStreams.length; i++) {
//            inStreams[i] = ne

        }

        int count = 0;
        ArrayList<String> jsons = new ArrayList<>();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println("File " + file.getName());
                Document doc = new Document();
//        Document doc = null;
                try {
                    String json = extractionServices.processWorkflow(file);
                    jsons.add(json);

                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, ex);

                }
            }
        }

        String jsn = "";
        for (String json : jsons) {
            jsn += json;
        }

        return jsn;
    }

    @RequestMapping(value = "/workflow/test2", method = RequestMethod.GET)
    public @ResponseBody
    String workflowTest2() {

        Response response = null;
        String retVal = "null";
        File initialFile = new File("D:\\Daniel\\Belgium_Work\\Develop\\github\\DarkaivRestApi\\Internet and Higher Education\\11.pdf");
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(initialFile);
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(ExtractorController.class.getName()).log(Level.SEVERE, null, ex);
        }

        File originFile = null;
        try {
            originFile = DarkaivRestUtils.writeInputFile(inputStream);

            if (originFile == null) {
                response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
            } else {
                // starts conversion process

                retVal = extractionServices.processWorkflow(originFile);

                DarkaivRestUtils.removeTempFile(originFile);

                if (!DarkaivRestUtils.isResultOK(retVal)) {
                    response = Response.status(Status.NO_CONTENT).build();
                } else {

                    response = Response.status(Status.OK).entity(retVal).type(MediaType.APPLICATION_JSON).build();

                }
            }
        } catch (NoSuchElementException nseExp) {
            logger.error("Could not get an engine from the pool within configured time. Sending service unavailable.");
            response = Response.status(Status.SERVICE_UNAVAILABLE).build();
        } catch (Exception exp) {
            logger.error("An unexpected exception occurs. ", exp);
            response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(exp.getMessage()).build();
        } finally {
            DarkaivRestUtils.removeTempFile(originFile);

        }

        return response.toString() + retVal + originFile.toString();
    }

    /**
     *
     * @param inputStream
     * @return
     */
    @Path("/workflow")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
//    public Response workflow(@FormDataParam(INPUT) InputStream inputStream) {
    public Response workflow(@FormDataParam("file") InputStream inputStream) {
//        String json = "";
////        Document doc = null;
//        try {
//            json = extractionServices.processWorkflow(file);
//        } catch (Exception ex) {
//            java.util.logging.Logger.getLogger(RestController.class.getName()).log(Level.SEVERE, null, ex);
//
//        }
//
////        return json;
        Response response = null;
        String retVal;

        File originFile = null;
        try {
            originFile = DarkaivRestUtils.writeInputFile(inputStream);

            if (originFile == null) {
                response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
            } else {
                // starts conversion process

                retVal = extractionServices.processWorkflow(originFile);

                DarkaivRestUtils.removeTempFile(originFile);

                if (!DarkaivRestUtils.isResultOK(retVal)) {
                    response = Response.status(Status.NO_CONTENT).build();
                } else {

                    response = Response.status(Status.OK).entity(retVal).type(MediaType.APPLICATION_JSON).build();

                }
            }
        } catch (NoSuchElementException nseExp) {
            logger.error("Could not get an engine from the pool within configured time. Sending service unavailable.");
            response = Response.status(Status.SERVICE_UNAVAILABLE).build();
        } catch (Exception exp) {
            logger.error("An unexpected exception occurs. ", exp);
            response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(exp.getMessage()).build();
        } finally {
            DarkaivRestUtils.removeTempFile(originFile);

        }

        return response;
        
    }

}
