/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vlir.darkaiv.rest.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.grobid.core.engines.Engine;
//import org.grobid.core.factory.GrobidFactory;
//import org.grobid.core.factory.GrobidPoolingFactory;
//import org.grobid.service.exceptions.GrobidServiceException;
//import org.grobid.core.utilities.GrobidProperties;
//import org.grobid.core.utilities.Utilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author daniel
 */
public class DarkaivRestUtils {

    /**
     * The class Logger.
     */
    private static final Logger logger = LoggerFactory
            .getLogger(DarkaivRestUtils.class);

    // type of PDF annotation for visualization purposes
    public enum Annotation {
        CITATION, BLOCK, FIGURE
    };

    /**
     * Check whether the result is null or empty.
     *
     * @param result the result of the process.
     * @return true if the result is not null and not empty, false else.
     */
    public static boolean isResultOK(String result) {
        return StringUtils.isBlank(result) ? false : true;
    }

    /**
     * Write an input stream in temp directory.
     *
     * @param inputStream
     * @return
     */
    public static File writeInputFile(InputStream inputStream) {
        logger.debug(">> set origin document for stateless service'...");

        File originFile = null;
        OutputStream out = null;
        try {
            originFile = newTempFile("origin", ".pdf");

            out = new FileOutputStream(originFile);

            byte buf[] = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (IOException e) {
            logger.error(
                    "An internal error occurs, while writing to disk (file to write '"
                    + originFile + "').", e);
            originFile = null;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                inputStream.close();
            } catch (IOException e) {
                logger.error("An internal error occurs, while writing to disk (file to write '"
                        + originFile + "').");
                originFile = null;
            }
        }
        return originFile;
    }

    /**
     * Creates a new not used temprorary file and returns it.
     *
     * @return
     */
    public static File newTempFile(String fileName, String extension) throws IOException {
        return File.createTempFile(fileName, extension);
    }

    /**
     * Delete the temporary file.
     *
     * @param file the file to delete.
     */
    public static void removeTempFile(final File file) {
        try {
            // sanity cleaning
//            to see which is the default tmp folder 
//            deleteOldies(GrobidProperties.getTempPath(), 300);
            logger.debug("Removing " + file.getAbsolutePath());
//            here it deletes the specificated file, so it should be sufficient!!
            file.delete();
        } catch (Exception exp) {
            logger.error("Error while deleting the temporary file: " + exp);
        }
    }

    private static boolean deleteOldies(File dir, int maxLifeInSeconds, boolean root) {
        Date currentDate = new Date();
        long currentDateMillisec = currentDate.getTime();
        boolean empty = true;
        boolean success = true;
        long threasholdMillisec = currentDateMillisec - (maxLifeInSeconds * 1000);
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            for (int i = 0; i < children.length; i++) {
                long millisec = children[i].lastModified();
                if (millisec < threasholdMillisec) {
                    success = deleteOldies(children[i], maxLifeInSeconds, false);
                    if (!success) {
                        return false;
                    }
                } else {
                    empty = false;
                }
            }
        }
        // if the dir is a file or if the directory is empty and it is no the root dir, we delete it
        if (!root && (empty || (!dir.isDirectory()))) {
            success = dir.delete();
        }
        return success;
    }

    /**
     * Delete temporary directory.
     *
     * @param path the path to the directory to delete.
     */
//    public static void removeTempDirectory(final String path) {
//        try {
//            logger.debug("Removing " + path);
//            File theDirectory = new File(path);
//            if (theDirectory.exists()) {
//                theDirectory.delete();
//            }
//        } catch (Exception exp) {
//            logger.error("Error while deleting the temporary directory: " + exp);
//        }
//    }
    /**
     * @return a new engine from GrobidFactory if the execution is parallel,
     * else return the instance of engine.
     */
//    public static Engine getEngine(boolean isparallelExec) {
//        return isparallelExec ? GrobidPoolingFactory.getEngineFromPool()
//                : GrobidFactory.getInstance().getEngine();
//    }
}
